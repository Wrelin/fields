<?php


class TextAreaFieldConfig extends FieldConfig
{
    protected static string $type = 'textarea';

    public function __construct(string $label)
    {
        parent::__construct($label);
    }
}