<?php


class FieldConfig
{
    private string $label;
    protected static string $type = 'text';

    public function __construct(string $label)
    {
        $this->label = $label;
    }

    public function getConfig()
    {
        return [
            'type' => static::$type,
            'label' => $this->label
        ];
    }
}