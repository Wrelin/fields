<?php


class RepeaterFieldConfig extends FieldConfig
{
    private int $minItems;
    private int $maxItems;
    private array $fieldConfigs;
    protected static string $type = 'repeater';

    public function __construct(string $label, array $fieldConfigs, int $minItems = 0, int $maxItems = 0)
    {
        parent::__construct($label);
        $this->minItems = $minItems;
        $this->maxItems = $maxItems;
        $this->fieldConfigs = $fieldConfigs;
    }

    public function getConfig()
    {
        return array_merge(
            parent::getConfig(),
            [
                'minItems' => $this->minItems,
                'maxItems' => $this->maxItems,
                'style' => 'collapsed',
                'form' => [
                    'fields' => [
                        //Тут надо подумать как затолкать сюда другие конфиги полей
                    ]
                ]
            ]
        );
    }
}