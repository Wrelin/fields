<?php


class FileUploadFieldConfig extends FileFieldConfig
{
    private bool $caption;
    protected static string $type = 'fileupload';

    public function __construct(string $label, string $mode, FieldSize $size, bool $caption)
    {
        parent::__construct($label, $mode, $size);
        $this->caption = $caption;
    }

    public function getConfig()
    {
        return array_merge(
            parent::getConfig(),
            [
                'caption' => $this->caption
            ]
        );
    }
}