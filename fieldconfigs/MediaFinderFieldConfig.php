<?php


class MediaFinderFieldConfig extends FileFieldConfig
{
    protected static string $type = 'mediafinder';

    public function __construct(string $label, string $mode, FieldSize $size)
    {
        parent::__construct($label, $mode, $size);
    }
}