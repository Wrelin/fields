<?php

abstract class FileFieldConfig extends FieldConfig
{
    private string $mode;
    protected FieldSize $size;

    public function __construct(string $label, string $mode, FieldSize $size)
    {
        parent::__construct($label);
        $this->mode = $mode;
        $this->size = $size;
    }

    public function getConfig()
    {
        return array_merge(
            parent::getConfig(),
            [
                'mode' => $this->mode,
                'size' => $this->size
            ]
        );
    }
}