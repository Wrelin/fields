<?php


class RichEditorFieldConfig extends FieldConfig
{
    protected static string $type = 'richeditor';

    public function __construct(string $label)
    {
        parent::__construct($label);
    }
}