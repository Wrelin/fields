<?php

require_once('fieldconfigs/FieldConfig.php');

class Field
{
    private string $code;
    private FieldConfig $config;

    public function __construct(string $code, FieldConfig $config)
    {
        $this->code = $code;
        $this->config = $config;
    }

    public function getField()
    {
        return [$this->code => $this->config->getConfig()];
    }
}