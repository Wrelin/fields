<?php


class FieldSize
{
    private int $width;
    private int $height;

    public function __construct(int $height, int $width)
    {
        $this->height = $height;
        $this->width = $width;
    }

    public function resize(int $newHeight, int $newWidth)
    {
        return new FieldSize($newHeight, $newWidth);
    }
}